package cn.antcore.resources.repository.git;

import cn.antcore.resources.repository.git.config.GitConfig;
import cn.antcore.resources.utils.StringUtils;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Git构建器
 * Created By Hong on 2018/8/13
 **/
public class GitBuilder {

    /**
     * UsernamePasswordCredentialsProvider Cache
     **/
    private static Map<String, UsernamePasswordCredentialsProvider> PROVIDER_CACHE = new ConcurrentHashMap<>();

    private static GitConfig GIT_CONFIG = GitConfig.get();

    /**
     * 构建Git验证器
     *
     * @return UsernamePasswordCredentialsProvider
     */
    public static UsernamePasswordCredentialsProvider createProvider() {
        if(StringUtils.isEmpty(GIT_CONFIG.getUsername()) || StringUtils.isEmpty(GIT_CONFIG.getPassword())) {
            return null;
        }
        if (PROVIDER_CACHE.containsKey(GIT_CONFIG.getUsername().concat(GIT_CONFIG.getPassword()))) {
            return PROVIDER_CACHE.get(GIT_CONFIG.getUsername().concat(GIT_CONFIG.getPassword()));
        }
        PROVIDER_CACHE.put(GIT_CONFIG.getUsername().concat(GIT_CONFIG.getPassword()), new UsernamePasswordCredentialsProvider(GIT_CONFIG.getUsername(), GIT_CONFIG.getPassword()));
        return PROVIDER_CACHE.get(GIT_CONFIG.getUsername().concat(GIT_CONFIG.getPassword()));
    }

    /**
     * 构建Git验证器
     *
     * @param username 用户名
     * @param password 密码
     * @return UsernamePasswordCredentialsProvider
     */
    public static UsernamePasswordCredentialsProvider createProvider(String username, String password) {
        if (StringUtils.isEmpty(username)) {
            throw new IllegalArgumentException("username is empty.");
        }
        if (StringUtils.isEmpty(password)) {
            throw new IllegalArgumentException("password is empty.");
        }
        if (PROVIDER_CACHE.containsKey(username.concat(password))) {
            return PROVIDER_CACHE.get(username.concat(password));
        }
        PROVIDER_CACHE.put(username.concat(password), new UsernamePasswordCredentialsProvider(username, password));
        return PROVIDER_CACHE.get(username.concat(password));
    }

}
