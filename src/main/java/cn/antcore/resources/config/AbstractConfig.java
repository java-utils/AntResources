package cn.antcore.resources.config;

import cn.antcore.resources.Resources;
import cn.antcore.resources.SuffixConstants;
import cn.antcore.resources.extend.PropertiesResources;
import cn.antcore.resources.extend.XmlResources;
import cn.antcore.resources.extend.YamlResources;

import java.io.IOException;

/**
 * Created by Hong on 2018/1/3.
 */
public abstract class AbstractConfig {

    private final static String[] resourcesNames = {"yml", "yaml", "properties", "xml"};

    /**
     * 获取资源文件
     *
     * @param name classpath 文件名
     * @return Resources
     * @throws IOException IOException
     */
    protected Resources getResources(String name) throws IOException {
        name = getResourcesName(name);
        if (name.endsWith(SuffixConstants.YAML) || name.endsWith(SuffixConstants.YML)) {
            //Yaml资源
            YamlResources resources = new YamlResources();
            resources.loadByClassPath(name);
            return resources;
        } else if (name.endsWith(SuffixConstants.PROPERTIES)) {
            //Properties资源
            PropertiesResources resources = new PropertiesResources();
            resources.loadByClassPath(name);
            return resources;
        } else if (name.endsWith(SuffixConstants.XML)) {
            //Xml资源
            XmlResources resources = new XmlResources();
            resources.loadByClassPath(name);
            return resources;
        }
        return null;
    }

    /**
     * 获取存在的资源名
     *
     * @param resourcesName 资源名（不含后缀）
     * @return 资源名
     */
    private String getResourcesName(String resourcesName) {
        for (String name : resourcesNames) {
            name = resourcesName + "." + name;
            if (getClass().getResourceAsStream("/" + name) != null) {
                return name;
            }
        }
        return "";
    }

}
