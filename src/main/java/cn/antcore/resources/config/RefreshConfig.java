package cn.antcore.resources.config;

/**
 * Created By Hong on 2018/7/30
 **/
public interface RefreshConfig {
    void refresh();
}
