package cn.antcore.resources.config;

import cn.antcore.resources.Constants;
import cn.antcore.resources.core.AutoResources;
import cn.antcore.resources.Resources;
import cn.antcore.resources.convert.impl.*;
import cn.antcore.resources.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

/**
 * Created by Hong on 2017/12/27.
 */
public class GlobalConfig extends AbstractConfig implements Config, RefreshConfig {

    private static final Logger LOG = LoggerFactory.getLogger(GlobalConfig.class);

    private static final Map<Object, Object> KVS = new Hashtable<>();
    private static final GlobalConfig CONFIG = new GlobalConfig();

    {
        /**
         * 初始化，加载默认的配置文件
         */
        try {
            Resources resources = super.getResources(Constants.INIT_CONFIG);
            if (resources != null && resources.getResources() != null) {
                CONFIG.putAll(resources.getResources());
            }
        } catch (IOException e) {
            if (LOG.isErrorEnabled()) {
                LOG.error("Init Config Fail.");
            }
        }
    }

    /**
     * 获取单例GlobalConfig
     *
     * @return Config
     */
    public static Config get() {
        return CONFIG;
    }

    /**
     * 读取配置资源集合
     *
     * @param var1 资源文件名集合
     * @return Config
     */
    public static Config readConfig(String[] var1) {
        CONFIG.read(var1);
        return CONFIG;
    }

    /**
     * 添加配置
     *
     * @param config 配置集合
     * @return Config
     */
    public static Config putAll(Map<Object, Object> config) {
        KVS.putAll(config);
        return CONFIG;
    }

    /**
     * 添加配置
     *
     * @param key   Key
     * @param value Value
     * @return Config
     */
    public static Config put(Object key, Object value) {
        KVS.put(key, value);
        return CONFIG;
    }

    /**
     * 封闭的构造函数
     */
    private GlobalConfig() {

    }

    /**
     * 读取配置资源
     *
     * @param var1 资源名
     */
    private void read(String[] var1) {
        if (var1 == null) {
            return;
        }
        for (String name : var1) {
            this.read(name);
        }
    }

    /**
     * 读取配置资源
     *
     * @param name 资源名
     */
    private void read(String name) {
        try {
            if (StringUtils.isEmpty(name)) {
                return;
            }
            Resources resources = new AutoResources(name.trim());
            KVS.putAll(resources.getResources());
        } catch (IOException e) {
        }
    }

    @Override
    public String getValue(String key) {
        return new StringConvert().convert(KVS.get(key));
    }

    @Override
    public Boolean getBooleanValue(String key) {
        return new BooleanConvert().convert(KVS.get(key));
    }

    @Override
    public Integer getIntegerValue(String key) {
        return new IntegerConvert().convert(KVS.get(key));
    }

    @Override
    public Float getFloatValue(String key) {
        return new FloatConvert().convert(KVS.get(key));
    }

    @Override
    public Double getDoubleValue(String key) {
        return new DoubleConvert().convert(KVS.get(key));
    }

    @Override
    public Character getCharValue(String key) {
        return new CharConvert().convert(KVS.get(key));
    }

    @Override
    public Map<Object, Object> getConfig() {
        return new HashMap<>(KVS);
    }

    @Override
    public void clear() {
        KVS.clear();
    }

    @Override
    public void refresh() {

    }
}

