package cn.antcore.resources.core.properties;

import cn.antcore.resources.Constants;
import cn.antcore.resources.KeyConstants;
import cn.antcore.resources.config.Config;
import cn.antcore.resources.config.GlobalConfig;
import cn.antcore.resources.db.DbUtils;
import cn.antcore.resources.db.select.Select;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Created By Hong on 2018/7/30
 **/
public class DbProperties extends Hashtable<Object, Object> {

    private static final Config CONFIG = GlobalConfig.get();

    /**
     * 创建没有默认资源的空资源
     */
    public DbProperties() {
        super();
    }

    /***
     * 创建有默认资源的非空资源
     * @param defaults  默认
     */
    public DbProperties(Properties defaults) {
        super(defaults);
    }

    public synchronized Object setProperty(String key, String value) {
        return super.put(key, value);
    }

    public synchronized void load() {
        this.load(CONFIG.getValue(KeyConstants.DB_TABLE_NAME));
    }

    public synchronized void load(String tableName) {
        String sql = new Select()
                .table(tableName)
                .columnAll()
                .toString();
        List<Map<String, Object>> list = DbUtils.select(sql);
        for (Map<String, Object> item : list) {
            this.put(item.get(Constants.DB_CONFIG_KEY), item.get(Constants.DB_CONFIG_VALUE));
        }
    }

}
