package cn.antcore.resources.core;

/**
 * Created By Hong on 2018/8/17
 **/
public abstract class AbstractConvertResourcesName {

    private final static String START_WITH = "/";

    /**
     * classpath转换成可用的包内路径
     *
     * @param var1 classpath路径
     * @return 包内正确路径
     */
    public String convert(String var1) {
        if (!var1.startsWith(START_WITH)) {
            var1 = START_WITH + var1;
        }
        return var1;
    }
}
