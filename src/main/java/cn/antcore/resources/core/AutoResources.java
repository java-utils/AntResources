package cn.antcore.resources.core;

import cn.antcore.resources.PrefixConstants;
import cn.antcore.resources.Resources;
import cn.antcore.resources.SuffixConstants;
import cn.antcore.resources.convert.impl.*;
import cn.antcore.resources.extend.*;
import cn.antcore.resources.utils.StringUtils;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

/**
 * 自动识别资源格式并读取
 * Created by Hong on 2017/12/27.
 */
public class AutoResources extends AbstractProfileName implements Resources {

    private Resources resources;

    public AutoResources(String name) throws IOException {
        super(name);
        //获取修正后的正确资源名
        if (PrefixConstants.DB.equals(super.getPrefixName())) {
            //来源 Db
            DbResources resources = new DbResources();
            if (StringUtils.isEmpty(super.getProfileName())) {
                resources.load();
            } else {
                resources.load(super.getProfileName());
            }
            this.resources = resources;
            return;
        } else if (PrefixConstants.GIT.equals(super.getPrefixName())) {
            //来源 Git
            GitResources resources = new GitResources();
            if (StringUtils.isEmpty(super.getProfileName())) {
                resources.load();
            } else {
                resources.load(super.getProfileName());
            }
            this.resources = resources;
            return;
        }

        if (SuffixConstants.YAML.equals(super.getSuffixName()) || SuffixConstants.YML.equals(super.getSuffixName())) {
            //Yaml资源
            YamlResources resources = new YamlResources();
            if (super.isClassFile(name)) {
                resources.loadByClassPath(super.getProfileName());
            } else {
                resources.loadByFilePath(super.getProfileName());
            }
            this.resources = resources;
        } else if (SuffixConstants.PROPERTIES.equals(super.getSuffixName())) {
            //Properties资源
            PropertiesResources resources = new PropertiesResources();
            if (super.isClassFile(name)) {
                resources.loadByClassPath(super.getProfileName());
            } else {
                resources.loadByFilePath(super.getProfileName());
            }
            this.resources = resources;
        } else if (SuffixConstants.XML.equals(super.getSuffixName())) {
            //Xml资源
            XmlResources resources = new XmlResources();
            if (super.isClassFile(name)) {
                resources.loadByClassPath(super.getProfileName());
            } else {
                resources.loadByFilePath(super.getProfileName());
            }
            this.resources = resources;
        }
    }

    @Override
    public Map<Object, Object> getResources() {
        if (resources != null) {
            return resources.getResources();
        }
        return Collections.emptyMap();
    }

    @Override
    public void writeLocalProperties() {
        resources.writeLocalProperties();
    }

    @Override
    public String getValue(String key) {
        return resources == null ? null : new StringConvert().convert(resources.getResources().get(key));
    }

    @Override
    public Boolean getBooleanValue(String key) {
        return resources == null ? null : new BooleanConvert().convert(resources.getResources().get(key));
    }

    @Override
    public Integer getIntegerValue(String key) {
        return resources == null ? null : new IntegerConvert().convert(resources.getResources().get(key));
    }

    @Override
    public Float getFloatValue(String key) {
        return resources == null ? null : new FloatConvert().convert(resources.getResources().get(key));
    }

    @Override
    public Double getDoubleValue(String key) {
        return resources == null ? null : new DoubleConvert().convert(resources.getResources().get(key));
    }

    @Override
    public Character getCharValue(String key) {
        return resources == null ? null : new CharConvert().convert(resources.getResources().get(key));
    }

    @Override
    public Map<Object, Object> getConfig() {
        return resources.getResources();
    }

    @Override
    public void clear() {
        this.resources.clear();
    }
}
