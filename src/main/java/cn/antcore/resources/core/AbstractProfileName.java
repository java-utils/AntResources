package cn.antcore.resources.core;

import cn.antcore.resources.KeyConstants;
import cn.antcore.resources.PrefixConstants;
import cn.antcore.resources.config.GlobalConfig;
import cn.antcore.resources.utils.StringUtils;

import java.io.File;

/**
 * profile资源名处理
 * Created By Hong on 2018/8/17
 **/
public abstract class AbstractProfileName extends AbstractConvertResourcesName {

    private String sourceName;

    public AbstractProfileName(String sourceName) {
        this.sourceName = sourceName;
    }

    /**
     * 获取前缀名
     *
     * @return classpath, file, db, git
     */
    public String getPrefixName() {
        int index = this.sourceName.indexOf(":");
        if (index > -1) {
            return this.sourceName.substring(0, index).toLowerCase();
        } else {
            return "";
        }
    }

    /**
     * 获取后缀名
     *
     * @return yml, yaml, xml, properties
     */
    public String getSuffixName() {
        int index = this.sourceName.lastIndexOf(".");
        if (index > -1) {
            return this.sourceName.substring(index + 1, this.sourceName.length()).toLowerCase();
        } else {
            return "";
        }
    }

    /**
     * 获取源文件名
     *
     * @return sourceName
     */
    protected String getSourceName() {
        return this.sourceName;
    }

    /**
     * 获取Profile修饰的文件名
     *
     * @return prifileName
     */
    protected String getProfileName() {
        String var1 = ResourcesUtils.getPropertiesName(this.sourceName);
        String var2 = ResourcesUtils.getProfileProperties(var1);
        String var3 = this.getPrefixName();
        String var4 = var1;
        if (exists(var2, PrefixConstants.CLASS.equals(var3))) {
            var4 = var2;
        } else if (exists(var2, !PrefixConstants.FILE.equals(var3))) {
            var4 = var2;
        }
        return var4;
    }

    /**
     * @param var1        文件路径
     * @param isClassPath 是否是包内文件
     * @return
     */
    private boolean exists(String var1, boolean isClassPath) {
        if (isClassPath) {
            //包内文件是否存在
            String name = super.convert(var1);
            return this.getClass().getResourceAsStream(name) != null;
        }
        //包外文件是否存在
        return new File(var1).exists();
    }

    /**
     * 是否是Classpath包内文件
     *
     * @param name
     * @return trur 是包内文件
     */
    protected boolean isClassFile(String name) {
        if (PrefixConstants.CLASS.equals(this.getPrefixName())) {
            return true;
        }
        return false;
    }


    /**
     * 工具类
     */
    private static final class ResourcesUtils {
        /**
         * 获取带profile的资源名
         *
         * @param var1
         * @return
         */
        public static String getProfileProperties(String var1) {
            String profile = GlobalConfig.get().getValue(KeyConstants.PROFILE);
            if (StringUtils.isEmpty(profile)) {
                return var1;
            }
            profile = "-" + profile;

            StringBuilder builder = new StringBuilder(var1);

            //如果文件名已经存在profile，就不继续后续操作了
            if (builder.indexOf(profile) > -1) {
                return builder.toString();
            }

            int index = builder.lastIndexOf(".");
            if (index == -1) {
                builder.append(profile);
            } else {
                builder.insert(index, profile);
            }
            return builder.toString();
        }

        /**
         * 去除前缀的文件名
         *
         * @param var1 带前缀的文件名
         * @return
         */
        public static String getPropertiesName(String var1) {
            if (var1.indexOf(":") > -1) {
                return var1.substring(var1.indexOf(":") + 1, var1.length());
            }
            return var1;
        }
    }

}
