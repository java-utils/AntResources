package cn.antcore.resources.extend;

import cn.antcore.resources.core.AbstractResources;
import cn.antcore.resources.core.properties.DbProperties;

import java.io.InputStream;
import java.util.Map;

/**
 * 
 * Created By Hong on 2018/7/30
 **/
public final class DbResources extends AbstractResources {

    public void load() {
        DbProperties properties = new DbProperties();
        properties.load();
        map.putAll(properties);
    }

    public void load(String tableName) {
        DbProperties properties = new DbProperties();
        properties.load(tableName);
        map.putAll(properties);
    }

    @Override
    public void load(InputStream is) {

    }

    @Override
    public Map<Object, Object> getConfig() {
        return map;
    }

    @Override
    public void clear() {
        map.clear();
    }
}
