package cn.antcore.resources.extend;

import cn.antcore.resources.core.AbstractResources;
import cn.antcore.resources.core.properties.GitProperties;

import java.io.InputStream;
import java.util.Map;

/**
 * 
 * Created By Hong on 2018/8/13
 **/
public final class GitResources extends AbstractResources {

    public void load() {
        GitProperties properties = new GitProperties();
        properties.load();
        map.putAll(properties);
    }

    public void load(String... name) {
        GitProperties properties = new GitProperties();
        properties.load(name);
        map.putAll(properties);
    }

    @Override
    public void load(InputStream is) {

    }

    @Override
    public Map<Object, Object> getConfig() {
        return map;
    }

    @Override
    public void clear() {
        map.clear();
    }
}
