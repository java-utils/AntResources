import cn.antcore.resources.KeyConstants;
import cn.antcore.resources.Resources;
import cn.antcore.resources.config.GlobalConfig;
import cn.antcore.resources.core.AutoResources;
import cn.antcore.resources.db.select.Select;
import cn.antcore.resources.extend.*;
import cn.antcore.resources.repository.RemoteRepository;
import cn.antcore.resources.repository.git.GitCore;

import java.io.IOException;

/**
 * Created by Hong on 2017/12/27.
 */
public class Test {

    @org.junit.Test
    public void loadProperties() throws IOException {
        PropertiesResources resources = new PropertiesResources();
        resources.loadByClassPath("application.properties");
        System.err.println(resources.getResources());
    }

    @org.junit.Test
    public void loadPropertiesByFilePath() throws IOException {
        PropertiesResources resources = new PropertiesResources();
        resources.loadByFilePath("D:\\config\\common.properties");
        System.err.println(resources.getResources());
    }

    @org.junit.Test
    public void loadXmlProperties() throws IOException {
        XmlResources resources = new XmlResources();
        resources.loadByClassPath("application.xml");
        System.err.println(resources.getResources());
    }

    @org.junit.Test
    public void loadXmlPropertiesByFilePath() throws IOException {
        XmlResources resources = new XmlResources();
        resources.loadByFilePath("D:\\config\\application.xml");
        System.err.println(resources.getResources());
    }

    @org.junit.Test
    public void loadYamlProperties() throws IOException {
        YamlResources resources = new YamlResources();
        resources.loadByClassPath("application.yml");
        for (Object key : resources.getResources().keySet()) {
            System.err.println(key + "：" + resources.getResources().get(key));
        }
    }

    @org.junit.Test
    public void loadDbProperties() {
        DbResources resources = new DbResources();
        resources.load();
        for (Object key : resources.getResources().keySet()) {
            System.err.println(key + "：" + resources.getResources().get(key));
        }
    }

    @org.junit.Test
    public void loadDbPropertiesByTableName() {
        DbResources resources = new DbResources();
        resources.load("tb_config");
        for (Object key : resources.getResources().keySet()) {
            System.err.println(key + "：" + resources.getResources().get(key));
        }
    }

    @org.junit.Test
    public void autoReadResources() throws IOException {
        //Resources resources = new AutoResources("db:tb_config");
        //Resources resources = new AutoResources("classpath:application.yml");
        Resources resources = new AutoResources("git:client");
        for (Object key : resources.getResources().keySet()) {
            System.err.println(key + "：" + resources.getResources().get(key));
        }
    }

    @org.junit.Test
    public void sqlBuilder() {
        Select select = new Select();
        select.table(GlobalConfig.get().getValue(KeyConstants.DB_TABLE_NAME))
                .columnAll();
        System.err.println(select);
    }

    @org.junit.Test
    public void testGit() {
        RemoteRepository remoteRepository = new GitCore();
        remoteRepository.cloneRepository();
        remoteRepository.pullRepository();
    }

    @org.junit.Test
    public void loadGit() {
        GitResources resources = new GitResources();
        resources.load("client-dev");
        for (Object key : resources.getResources().keySet()) {
            System.err.println(key + "：" + resources.getResources().get(key));
        }
    }
}
